module.exports = {
  basePath: "/roti-calculator",
  assetPrefix: "/roti-calculator",
  publicRuntimeConfig: {
    // Will be available on client
    prefix: "/roti-calculator",
  },
};
