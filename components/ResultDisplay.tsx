import commonStyles from "../styles/ResultDisplay.module.scss";

export interface ResultDisplayProps {
  votes: Array<number>;
}

const ResultDisplay = ({ votes }: ResultDisplayProps) => {
  const getAverage = (votes: Array<number>): number => {
    let sum = 0;
    votes.map((vote) => {
      sum = sum + vote;
    });
    return Math.round((sum / votes.length) * 10) / 10;
  };

  return (
    <div className={commonStyles.container}>
      <div className={commonStyles.valueContainer}>
        <div className={commonStyles.value}>{getAverage(votes) || ""}</div>
        <div className={commonStyles.divider}>/5</div>
      </div>
      <div className={commonStyles.numberOfVoters}>
        Number of voters : {votes.length}
      </div>
    </div>
  );
};

export default ResultDisplay;
