import { useState } from "react";
import commonStyles from "../styles/RotiCalculator.module.scss";
import ResultDisplay from "./ResultDisplay";
import VoteGrid from "./VoteGrid";
import VotesDisplay from "./VotesDisplay";
import Actions from "./Actions";

const RotiCalculator = () => {
  const [votes, setVotes] = useState<Array<number>>([]);
  const onVote = (voteValue: number): void => {
    const newVotesValues = [...votes];
    newVotesValues.push(voteValue);
    setVotes(newVotesValues);
  };

  const onBackInVoteHistory = (): void => {
    const newVotesValues = [...votes];
    newVotesValues.pop();
    setVotes(newVotesValues);
  };

  const onClearVotes = (): void => {
    setVotes([]);
  };

  return (
    <>
      <VotesDisplay votes={votes} />
      <div className={commonStyles.resultActionsContainer}>
        <ResultDisplay votes={votes} />
        <Actions onBack={onBackInVoteHistory} onClear={onClearVotes} />
      </div>
      <VoteGrid onVote={(voteValue) => onVote(voteValue)} />;
    </>
  );
};

export default RotiCalculator;
